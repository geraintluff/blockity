The goal of this tool is to generate images that look a little bit like pixel art.  The idea is to use a limited number of colours, with a bias towards regions of the same colour (so, no gradients or dithering).

![Example](example.png)

## Usage

```
node blockity.js <input-image> <?output-png>
```

### Options

* `--pixels` - pixel count (default 10000)
* `--seeds` - number of "seeds" (default 200)
* `--scaling` - output scaling (default 4)

## How it works

At a high level:

* select some starting "seeds" (points on the image)
* grow outwards from the seeds until the image is covered

The distance metric between any two points is the product of the spatial distance and the RGB distance (both Euclidean).

### Selecting seeds

Seeds are selected to be "far apart" (using the above metric, which includes colour).  This is done by selecting 100 points for each seed, and choosing the one furthest away from the seeds already selected.

### Growing outwards

A list of "edge" pixels is kept (initially just the seeds).  For each, the distance to the nearest seed is stored.

The closest edge pixel is converted to be exactly the same colour as its seed, and the edge is expanded (in up to four directions), adding the new pixels to the edge list.  This is repeated until there are no edges left (i.e. the whole image is processed).