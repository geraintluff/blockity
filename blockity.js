var fs = require('fs');
var ndarrayZeros = require('zeros');
var getPixels = require('get-pixels');
var savePixels = require('save-pixels');

var api = module.exports = function (inputFile, outputFile, options, callback) {
	var pointCount = options.seeds || 1000;
	var maxPixels = options.pixels || 10000;
	var scaleUp = options.scale || 4;

	getPixels(inputFile, function (error, imagePixels) {
		if (error) return callback(error);
		
		var width = imagePixels.shape[0];
		var height = imagePixels.shape[1];
		var channels = imagePixels.shape[2];

		var pixelCount = width*height;
		var scalingFactor = Math.min(1, Math.sqrt(maxPixels/pixelCount)); // Scale to ~10000 pixels
		width = Math.floor(width*scalingFactor);
		height = Math.floor(height*scalingFactor);
		
		var pixels = ndarrayZeros([width, height, channels]);
		var blurPixels = ndarrayZeros([width, height, channels]);
		for (var x = 0; x < width; x++) {
			for (var y = 0; y < height; y++) {
				var trueX = Math.floor(Math.min(imagePixels.shape[0] - 1, x/scalingFactor));
				var trueY = Math.floor(Math.min(imagePixels.shape[1] - 1, y/scalingFactor));
				for (var i = 0; i < channels; i++) {
					var total = 0, totalCount = 0;
					for (var x2 = trueX; x2 < imagePixels.shape[0] && x2*scalingFactor <= x + 1; x2++) {
						for (var y2 = trueY; y2 < imagePixels.shape[1] && y2*scalingFactor <= y + 1; y2++) {
							total += imagePixels.get(trueX, trueY, i);
							totalCount++;
						}
					}
					pixels.set(x, y, i, total/totalCount);
					blurPixels.set(x, y, i, total/totalCount);
				}
			}
		}
		imagePixels = null;

		function colourDistance(x1, y1, x2, y2) {
			var dist2 = 0;
			for (var i = 0; i < channels; i++) {
				var v1 = pixels.get(x1, y1, i), v2 = pixels.get(x2, y2, i);
				var diff = (v1 - v2);
				dist2 += diff*diff;
			}
			return dist2; // Don't bother to sqrt, it's fine
		}
		function pixelDistance(x1, y1, x2, y2) {
			var colourDist = Math.sqrt(colourDistance(x1, y1, x2, y2));
			var geoDist = Math.sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
			return colourDist*geoDist;
		}
		function pointDistance(x1, y1, x2, y2) {
			return pixelDistance(x1, y1, x2, y2);
		}
		function compareDist(point1, point2) {
			return point1.dist - point2.dist + (Math.random() - 0.5)*0.1;
		}
		
		var points = [];
		var pointSample = 100;
		while (points.length < pointCount) {
			var newPoints = [];
			while (newPoints.length < pointSample) {
				var x = Math.floor(Math.random()*width);
				var y = Math.floor(Math.random()*height);
				newPoints.push({
					x: x,
					y: y
				});
			}
			// Sort by closeness to existing points
			newPoints.forEach(function (point, pointIndex) {
				var minDistance = 1e6;
				points.forEach(function (other, otherIndex) {
					var dist = pointDistance(other.x, other.y, point.x, point.y);
					minDistance = Math.min(dist, minDistance);
				});
				point.pointDist = minDistance;
			});
			newPoints.sort(function (a, b) {
				return a.pointDist - b.pointDist;
			});
			points.push(newPoints.pop());

			process.stdout.clearLine();
			process.stdout.cursorTo(0);
			process.stdout.write(points.length + ' points (target ' + pointCount + ')');
		}
		while (points.length < pointCount) {
			var x = Math.floor(Math.random()*width);
			var y = Math.floor(Math.random()*height);
			points.push({
				x: x,
				y: y
			});
		}
		console.log(' - done');
		
		var handled = [];
		for (var x = 0; x < width; x++) {
			handled[x] = [];
			for (var y = 0; y < height; y++) {
				handled[x][y] = false;
			}
		}
		
		var candidates = points.map(function (point) {
			return handled[point.x][point.y] = {
				x: point.x,
				y: point.y,
				pointX: point.x,
				pointY: point.y,
				dist: 0
			};
		});
		
		var pixelTotal = width*height;
		var pixelCount = 0;
		console.log(pixelTotal + ' pixels');
		var newCandidates = [];
		while (candidates.length) {
			var candidate = candidates.shift();
			if (handled[candidate.x][candidate.y] !== candidate) {
				continue;
			}
			handled[candidate.x][candidate.y] = true;
			for (var i = 0; i < channels; i++) {
				pixels.set(candidate.x, candidate.y, i, pixels.get(candidate.pointX, candidate.pointY, i));
			}
			if (candidate.x > 0) {
				newCandidates.push({
					x: candidate.x - 1,
					y: candidate.y,
					pointX: candidate.pointX,
					pointY: candidate.pointY,
					dist: colourDistance(candidate.x - 1, candidate.y, candidate.pointX, candidate.pointY)
				});
			}
			if (candidate.x + 1 < width) {
				newCandidates.push({
					x: candidate.x + 1,
					y: candidate.y,
					pointX: candidate.pointX,
					pointY: candidate.pointY,
					dist: colourDistance(candidate.x + 1, candidate.y, candidate.pointX, candidate.pointY)
				});
			}
			if (candidate.y > 0) {
				newCandidates.push({
					x: candidate.x,
					y: candidate.y - 1,
					pointX: candidate.pointX,
					pointY: candidate.pointY,
					dist: colourDistance(candidate.x, candidate.y - 1, candidate.pointX, candidate.pointY)
				});
			}
			if (candidate.y + 1 < height) {
				newCandidates.push({
					x: candidate.x,
					y: candidate.y + 1,
					pointX: candidate.pointX,
					pointY: candidate.pointY,
					dist: colourDistance(candidate.x, candidate.y + 1, candidate.pointX, candidate.pointY)
				});
			}
			pixelCount++;
			while (newCandidates.length) {
				var newCandidate = newCandidates.shift();
				var existing = handled[newCandidate.x][newCandidate.y];
				if (!existing || newCandidate.dist < existing.dist) {
					handled[newCandidate.x][newCandidate.y] = newCandidate;
					// Insertion sort
					var distValue = newCandidate.dist;
					var lowIndex = 0, highIndex = candidates.length, midIndex = (lowIndex + highIndex) >> 1;
					while (lowIndex !== midIndex) {
						if (distValue > candidates[midIndex].dist) {
							lowIndex = midIndex;
						} else {
							highIndex = midIndex;
						}
						midIndex = (lowIndex + highIndex) >> 1;
					}
					candidates.splice(lowIndex, 0, newCandidate);
				}
			}
			
			if (!(pixelCount%1000)) {
				var percent = Math.floor(pixelCount/pixelTotal*100);
				process.stdout.clearLine();
				process.stdout.cursorTo(0);
				process.stdout.write(percent + '%' + ' ' + candidates.length);
			}
		}

		var oldPixels = pixels;
		var pixels = ndarrayZeros([width*scaleUp, height*scaleUp, channels]);
		for (var x = 0; x < pixels.shape[0]; x++) {
			for (var y = 0; y < pixels.shape[1]; y++) {
				for (var i = 0; i < channels; i++) {
					pixels.set(x, y, i, oldPixels.get(Math.floor(x/scaleUp), Math.floor(y/scaleUp), i));
				}
			}
		}
		
		// Output result;
		console.log('');
		var imageData = savePixels(pixels, 'png');
		var stream = fs.createWriteStream(outputFile);
		imageData.pipe(stream);
		stream.on('error', function (error) {
			return callback(error);
		});
		stream.on('end', function () {
			callback(null);
		});
	});
};

if (require.main === module) {
	var options = {
		seeds: 200,
		pixels: 10000,
		scale: 4
	};
	var args = [];
	for (var i = 2; i < process.argv.length; i++) {
		var item = process.argv[i];
		if (item[0] === '-') {
			var key = item.replace(/^\-+/, '');
			var value = process.argv[++i];
			if (typeof options[key] === 'number') {
				value = parseFloat(value);
			}
			options[key] = value;
		} else {
			args.push(item);
		}
	}
	var inFile = args[0], outFile = args[1];

	if (!inFile) {
		console.error('Usage:\n\rnode blockity.js <input> <?output>');
		process.exit(1);
	}
	outFile = outFile || (inFile.replace(/\.[^.]*$/, '') + '.blockity.png');
	
	api(inFile, outFile, options, function (error) {
		if (error) throw error;
		console.log('Wrote to: ' + outFile);
	});
}